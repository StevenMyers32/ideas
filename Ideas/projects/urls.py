from django.conf.urls            import patterns, url
from django.views.generic        import DetailView, ListView
from django.views.generic.simple import direct_to_template, redirect_to
from Ideas.projects              import views
from Ideas.projects.models       import Project

urlpatterns = patterns('',
	# Default projects page shows lists of projects 
	url(r'^$', 
		ListView.as_view(
			queryset=Project.objects.all().order_by('-mod_date')[:50],
			context_object_name='project_list',
			template_name='projects/index.html'
		),
		name='index'
	),
	
	# Shows project details (ex: 3/)
	url(r'^(?P<pk>\d+)/$', 
		DetailView.as_view(
			model=Project,
			template_name='projects/details.html'
		),
		name='details'
	),
	
	# Shows the create project page (ex: create/)
	url(r'^create/$', views.create, name='create')
)
