from django.shortcuts           import HttpResponseRedirect, render
from django.contrib.auth.models import User
from Ideas.projects.models      import Project
from Ideas.projects.forms		import CreateForm

def create(request):
	"""
	Creates a new project entry
	"""
	if (request.method == 'POST') :
		create_form = CreateForm(request.POST)
		if create_form.is_valid():
			try:
				# Get the parameters from the form
				in_title = request.POST['title']
				in_platform = request.POST['platform']
				in_description = request.POST['description']
			except KeyError:
				# For now do nothing
				pass
			else:
				# Verify the contents
				
				# # Placeholder to get the first user
				in_user = User.objects.all()[0]
				
				
				# Add the new project to the database
				new_project = Project(
					title = in_title,
					user = in_user,
					platform = in_platform,
					description = in_description		
				)
				new_project.save()
		
				# Redirect to the Project list page
				return HttpResponseRedirect('/projects/')
	else:		
		create_form = CreateForm()
		
	return render(request, 'projects/create.html', {
		'create_form': create_form
	})
	
	"""
	try:
		# Get the parameters from the form
		in_title = request.POST['title']
		in_platform = request.POST['platform']
		in_description = request.POST['description']
	except KeyError:
		return render(request, 'projects/create.html', {
			'error_message' : 'Caught an error while processing the form'
		})
	else:
		# Verify the contents
		
		# # Placeholder to get the first user
		in_user = User.objects.all()[0]
		
		
		# Add the new project to the database
		new_project = Project(
			title = in_title,
			user = in_user,
			platform = in_platform,
			description = in_description		
		)
		new_project.save()
		
		# Return to the project list page
		return HttpResponseRedirect('/projects/')
	"""