from django.db                  import models
from django.contrib.auth.models import User

class Project(models.Model):
	"""
	Defines the Projects for idea submission
	"""
	title         = models.CharField(max_length=35)
	user          = models.ForeignKey(User)
	
	# Platform can be optional
	platform      = models.CharField(max_length=35, blank=True)
	
	# Long description (The default form widget for this field is a Textarea.)
	description   = models.TextField()
	
	# Modified date
	mod_date = models.DateTimeField(auto_now=True)
	
	#For printing
	def __unicode__(self):
		return self.title